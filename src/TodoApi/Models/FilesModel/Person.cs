﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TodoApi.Models.FilesModel
{
    public class Person : EntityBase
    {
        //[Index(IsUnique = true)]
        public int RUT { get; set; }

        public int Rol { get; set; }

    }
}
