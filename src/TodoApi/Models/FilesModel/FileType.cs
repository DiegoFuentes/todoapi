﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Models.FilesModel
{
    public class FileType
    {
        public Application App { get; set; }
        public string Name { get; set; }
    }
}
