﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Models.FilesModel
{
    public class PersonFiles
    {
        public virtual Person Person{ get; set; }
        public virtual List<File> Files{ get; set; }
    }
}
