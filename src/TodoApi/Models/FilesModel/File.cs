﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Models.FilesModel
{
    public class File : EntityBase
    {
        public FileType Type { get; set; }
        public bool MandatoryForMe { get; set; }
        public bool IsPresented { get; set; }
        public bool IsUploaded { get; set; }
        public string Uri { get; set; }
    }
}
